package testUnitarios;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import operation.OperationType;

public class OperationTypeTest {
	@Test
	public void testOperationTypeValueOfBuyOk() {
		assertTrue(OperationType.valueOf("BUY")==OperationType.BUY);
	}
	@Test
	public void testOperationTypeValueOfSellOk() {
		assertTrue(OperationType.valueOf("SELL")==OperationType.SELL);
	}
	@Test
	public void testOperationTypeValuesOk() {
		OperationType[] values = {OperationType.BUY, OperationType.SELL};
		assertTrue(OperationType.values()[0]==values[0]);
		assertTrue(OperationType.values()[0]==values[0]);
		assertTrue(OperationType.values().length==2);
	}
}
