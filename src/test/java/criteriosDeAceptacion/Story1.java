package criteriosDeAceptacion;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import asset.Asset;
import bot.Bot;
import bot.BotListener;
import market.Market;
import mock.MarketFake;
import operation.OperationType;
import rules.AssetRule;
import wallet.Wallet;

public class Story1 {
	private Bot bot;
	private Asset asset;
	private List<AssetRule> activeRules;

	@Mock
	Market market;
	
	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.asset = new Asset("BITCOIN","BTC");
		activeRules= new ArrayList<AssetRule>();
	}
	
	@Test
	public void testOperateRulesNotExist() {
		createBot();
		bot.operate();
		assertTrue(bot.getRules().size() == 0);
	}
	
	@Test
	public void testOperateDecreasedMoreThanTheRule() {
		addRule(OperationType.BUY);
		createBot();
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(94.0);
		bot.operate();
		assertTrue(bot.getRules().size() == 0);
	}
	
	@Test
	public void testOperateRule1DecreasedTheSameThanTheRule() {
		addRule(OperationType.BUY);
		addRule(OperationType.SELL);
		createBot();
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(95.0);
		bot.operate();
		assertTrue(bot.getRules().size() == 1);
		assertTrue(bot.getRules().get(0).getOperationType() == OperationType.SELL);
	}
	
	@Test
	public void testOperateWithTwoRulesPriceNotChanged() {
		addRule(OperationType.BUY);
		addRule(OperationType.SELL);
		createBot();
		bot.operate();
		assertTrue(bot.getRules().size() == 2);
	}

	private void createBot() {
		Wallet wallet = new Wallet();
		wallet.addAmount(500);
		bot= new Bot(wallet, activeRules, 0, new MarketFake());
		@SuppressWarnings("unused")
		BotListener botListener = new BotListener(bot);
	}
	
	private void addRule(OperationType type)
	{
		activeRules.add(new AssetRule(1, asset,1, type, 100));
	}
}
