package criteriosDeAceptacion;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import asset.Asset;
import bot.Bot;
import bot.BotListener;
import market.Market;
import mock.MarketFake;
import operation.OperationType;
import rules.AssetRule;
import wallet.Wallet;

public class Story4 {
	private Bot bot;
	private Wallet wallet;
	private Asset asset;
	private List<AssetRule> activeRules;

	@Mock
	Market market;
	
	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.asset = new Asset("BITCOIN","BTC");
		activeRules= new ArrayList<AssetRule>();
	}
	
	@Test
	public void testPlaceBuyOrderSuccessfully() {
		addRule(OperationType.BUY, 200);
		createBot();
		assertTrue(wallet.getMoney()==500);
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(150.0);
		fake.setCodeOrder("BASDGF3245");
		bot.operate();
		System.out.println(wallet.getMoney());
		assertTrue(wallet.getMoney()==350);
	}
	
	@Test
	public void testPlaceBuyOrderFail() {
		addRule(OperationType.BUY, 200);
		createBot();
		assertTrue(wallet.getMoney()==500);
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(150.0);
		fake.setCodeOrder(null);
		bot.operate();
		assertTrue(wallet.getMoney()==500);
	}
	
	@Test
	public void testPlaceSellOrderSuccessfully() {
		addRule(OperationType.SELL, 100);
		createBot();
		assertTrue(wallet.getMoney()==500);
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(150.0);
		fake.setCodeOrder("FST345135");
		bot.operate();
		assertTrue(wallet.getMoney()==650);
	}
	
	@Test
	public void testPlaceSellOrderFail() {
		addRule(OperationType.SELL, 100);
		createBot();
		assertTrue(wallet.getMoney()==500);
		MarketFake fake = (MarketFake) bot.getMarket();
		fake.setPrice(150.0);
		fake.setCodeOrder(null);
		bot.operate();
		assertTrue(wallet.getMoney()==500);
	}

	private void createBot() {
		wallet = new Wallet();
		wallet.addAmount(500);
		bot= new Bot(wallet, activeRules, 0, new MarketFake());
		@SuppressWarnings("unused")
		BotListener botListener = new BotListener(bot);
	}
	
	private void addRule(OperationType type, double referencePrice)
	{
		activeRules.add(new AssetRule(1, asset,1, type, referencePrice));
	}
}
