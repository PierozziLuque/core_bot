package rules;

import operation.OperationType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import asset.Asset;



public class AssetRule {
	private double deltaPercentage;
	private Asset asset;
	private double quantity;
	private OperationType operationType;
	private double referencePrice;
	
	public AssetRule
	(
		double deltaPercentage, 
		Asset asset, 
		double quantity, 
		OperationType operationType, 
		double referencePrice
	) {
		super();
		this.deltaPercentage = deltaPercentage;
		this.asset = asset;
		this.quantity = quantity;
		this.operationType = operationType;
		this.referencePrice = referencePrice;
	}
	
	public static boolean check(AssetRule rule, double marketPrice) {
		boolean isBuy = rule.isBuy();
		double referencePrice = rule.getReferencePrice();
		if( (isBuy && referencePrice < marketPrice) 
				|| 
			(!isBuy && marketPrice < referencePrice))
		{
			return false;
		}

		double diff = isBuy ? referencePrice - marketPrice :
			marketPrice - referencePrice;
		
		double percentage = Math.abs((diff/referencePrice)*100);
		return percentage >= rule.getDeltaPercentage();
	}

	public double getDeltaPercentage() {
		return deltaPercentage;
	}
	
	public double getReferencePrice() {
		return referencePrice;
	}

	public Asset getAsset() {
		return new Asset(this.asset.getName(),this.asset.getSymbol());
	}
	
	public String getSymbol() {
		return asset.getSymbol();
	}

	public OperationType getOperationType() { return this.operationType; }

	public double getQuantity() {
		return quantity;
	}
	
	public boolean isBuy()
	{
		return getOperationType() == OperationType.BUY;
	}	
	
	public void setReferencePrice(double price)
	{
		if(price<0)
			throw new IllegalArgumentException();
		this.referencePrice = price;
	}
	
	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
 
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

}
