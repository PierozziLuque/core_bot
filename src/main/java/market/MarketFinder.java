package market;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MarketFinder {
	private List<String> names;
	private Set<Market> markets;

	public MarketFinder() {
		names = new ArrayList<String>();
		markets = new HashSet<>();
	};

	public void initialize(String _path, String _package) throws MalformedURLException {
		try {
			for (File f : new File(_path).listFiles()) {
				if (isClassFile(f)) {
					try {
						Class<?> c = Class.forName(_package + "." + f.getName().replace(".class", ""));

						if (isMarket(c)) {
							Market market = new MarketProxy((Market) c.getDeclaredConstructor().newInstance());
							markets.add(market);
							names.add(market.getName());
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	public Market getMarket(String name) {
		Iterator<Market> it = markets.iterator();
		while (it.hasNext()) {
			Market market = it.next();
			if (market.getName().equals(name)) {
				return market;
			}
		}
		return null;
	}

	public List<String> getNames() {
		return names;
	}

	public Set<Market> getMarkets() {
		return markets;
	}

	private static boolean isClassFile(File f) {
		return (!f.getName().endsWith(".class")) ? false : true;
	}

	private static boolean isMarket(Class<?> c) throws ClassNotFoundException {
		if (!Market.class.isAssignableFrom(c))
			return false;
		else
			return true;
	}
}
