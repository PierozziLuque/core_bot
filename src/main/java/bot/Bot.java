package bot;

import market.Market;
import operation.OperationType;
import operation.Order;
import rules.AssetRule;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

import timer.Timer;
import wallet.Wallet;

public class Bot {
	private List<AssetRule> rules;
	private Market market;
	private Wallet wallet;
	private Timer timer;
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

	public Bot(Wallet wallet, List<AssetRule> rules, int loopTime, Market market) {
		this.rules = rules;
		this.wallet = wallet;
		this.market = market;
		this.timer = new Timer(loopTime);
	}

	public void start() {
		timer.start();
		while (rules.size() > 0) {
			this.operate();
		}
	}

	public void operate() {
		for (int i = 0; i < rules.size(); i++) {
			AssetRule rule = rules.get(i);
			String orderCode = null;
			double marketPrice = market.getPrice(rule.getSymbol());
			if (mustCreateOrder(rule, marketPrice)) {
				Order order = createOrder(marketPrice, rule);
				changes.firePropertyChange("orderCreated", rules.indexOf(rule), order);
				orderCode = market.placeOrder(order);
			}
			if(orderCode == null) return;
			OperationType type = rule.getOperationType();
			if(type == OperationType.BUY)
			{
				wallet.withdrawAmount(marketPrice * rule.getQuantity());
			}
			if(type == OperationType.SELL)
			{
				wallet.addAmount(marketPrice * rule.getQuantity());
			}
		}
	}

	public boolean mustCreateOrder(AssetRule rule, double marketPrice) {
		if (rule.isBuy() && wallet.getMoney() < (rule.getQuantity() * marketPrice) || marketPrice == -1) {
			return false;
		}
		return AssetRule.check(rule, marketPrice) ? true : false;
	}

	public void removeRule(int index) {
		if (!checkIndex(index)) {
			return;
		}
		rules.remove(index);
	}

	public Market getMarket() {
		return market;
	}

	public List<AssetRule> getRules() {
		return rules;
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}

	private boolean checkIndex(int index) {
		return index >= 0 && index < rules.size();
	}

	private Order createOrder(double marketPrice, AssetRule assetRule) {
		return new Order(marketPrice, new Date(), assetRule.getQuantity(), assetRule.getAsset(),
				assetRule.getOperationType());
	}
}
