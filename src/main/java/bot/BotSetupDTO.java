package bot;

import java.io.Serializable;
import java.util.List;

import rules.AssetRule;

public class BotSetupDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private int loopTime;
	private String _package;
	private String _path;
	private double amount;
	private List<AssetRule> rules;
	
	public BotSetupDTO(int loopTime, double amount, List<AssetRule> rules, String _path, String _package)
	{
		this.loopTime = loopTime;
		this.amount = amount;
		this.rules = rules;
		this._package = _package;
		this._path = _path;
		this.amount = amount;
	}

	public int getLoopTime() {
		return loopTime;
	}
	
	public String getPath() {
		return _path;
	}
	
	public String getPackage() {
		return _package;
	}

	public void setLoopTime(int loopTime) {
		this.loopTime = loopTime;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public void setPackage(String _package){
		this._package = _package;
	}
	
	public void setPath(String _path){
		this._path = _path;
	}
	
	public List<AssetRule> getRules() {
		return rules;
	}

	public void setRules(List<AssetRule> rules) {
		this.rules = rules;
	}

}
